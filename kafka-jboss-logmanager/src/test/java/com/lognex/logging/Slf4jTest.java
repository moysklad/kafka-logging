package com.lognex.logging;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.UUID;

public class Slf4jTest {

    static {
        System.setProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager");
    }

    @Test
    public void testSlf4jInterface() throws Exception {
        MDC.put("account_id", new UUID(0L, 100L).toString());
        Logger logger = LoggerFactory.getLogger(Slf4jTest.class);
        logger.debug("gazonk {}" , 1);
        logger.info("hello!");
        logger.error("aaa!", new Exception());
        MDC.clear();
    }
}
