package com.lognex.logging;

import org.jboss.logmanager.Level;
import org.jboss.logmanager.Logger;
import org.jboss.logmanager.MDC;
import org.junit.Test;

import java.util.UUID;

public class LogManagerTest {

    static {
        System.setProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager");
    }

    @Test
    public void testJbossLogManagerInterface() throws Exception {
        MDC.put("account_id", new UUID(0L, 100L).toString());
        Logger logger = Logger.getLogger(LogManagerTest.class.getName());
        logger.log(Level.INFO, "foobar {0}", 1);
        MDC.clear();
    }
}
