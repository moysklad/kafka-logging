package com.lognex.logging;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class JsonWriterTest {

    @Test
    public void test() throws IOException {
        JsonWriter jw = new JsonWriter();
        jw.writeObjectOpen();
        jw.writeStringField("foo", "123");
        jw.writeStringField("bar", "\"文字 Селёдка \n \u0011");
        jw.writeObjectClose();
        assertEquals("{\"foo\":\"123\",\"bar\":\"\\\"文字 Селёдка \\n \\u0011\"}", jw.toString());
    }
}
