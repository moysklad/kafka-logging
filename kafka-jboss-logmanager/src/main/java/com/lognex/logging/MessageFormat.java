package com.lognex.logging;

import org.jboss.logmanager.ExtLogRecord;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.time.Instant;

final class MessageFormat {
    private MessageFormat() {}

    private static final String ENCODING = "UTF8";

    public static byte[] key(String env,
                             String app,
                             String host) throws UnsupportedEncodingException {
        return (env + "|" + app + "|" + host).getBytes(ENCODING);
    }

    public static byte[] body(ExtLogRecord record,
                              String env,
                              String app,
                              String host,
                              String[] mdcKeyArray) throws UnsupportedEncodingException {
        JsonWriter jw = new JsonWriter();
        jw.writeObjectOpen();
        jw.writeStringField("@timestamp", Instant.ofEpochMilli(record.getMillis()).toString()); // ELK friendly name
        jw.writeStringField("env", env);
        jw.writeStringField("app", app);
        jw.writeStringField("host", host);
        jw.writeStringField("level", record.getLevel().getName());
        jw.writeStringField("logger", record.getLoggerName());
        jw.writeStringField("thread", record.getThreadName());
        for (String mdcKey : mdcKeyArray) {
            String mdcValue = record.getMdc(mdcKey);
            if (mdcValue != null) {
                jw.writeStringField(mdcKey, mdcValue);
            }
        }
        jw.writeStringField("message", record.getFormattedMessage());
        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            record.getThrown().printStackTrace(pw);
            pw.close();
            jw.writeStringField("stackTrace", sw.toString());
        }
        jw.writeObjectClose();
        return jw.toString().getBytes(ENCODING);
    }
}
