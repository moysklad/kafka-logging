### Kafka-logging

Реализация аппендеров в apache kafka для различных имплементаций логирования.

* **kafka-jboss-logmanager** - для jboss logmanager. Jboss logmanager - это своя реализация логирования в jboss7/wildfly.
Заменить ее на что-то более стандартное без костылей нельзя (см. <https://issues.jboss.org/browse/WFCORE-2995>). Проще написать аппендер.
* **kafka-logback** - для logback.

Аппендеры никогда не блокируют логирующий поток. Отправка сообщений в kafka идет через внутреннюю очередь, при
переполнении записи лога отбрасываются.

В kafka отправляется json следующего формата:
```json
{
  "@timestamp":"2018-10-05T12:39:16.315Z",
  "appname":"hoard",
  "hostname":"online.moysklad.ru",
  "level":"INFO",
  "logger":"com.hazelcast.instance.Node",
  "thread":"shutdown-hook",
  "account_id":"00000000-0000-0000-0000-000000000064",
  "message":"[172.18.13.1]:5801 [hoard] [3.8.6] Shutting down multicast service...",
  "stackTrace":"java.lang.Exception\n\tat com.lognex.logging.Slf4jTest.testSlf4jInterface(Slf4jTest.java:22)\n"
}
```
account_id - это поле из Mapped Diagnostic Context (см. <https://logback.qos.ch/manual/mdc.html>).

#### Конфигурация
* **appname** - имя приложения
* **hostname** - имя хоста, с которого было отослано сообщение
* **bootstrapServers** - адреса kafka брокеров
* **topicName** - имя kafka топика
* **compressionType** - *none*, *lz4*, *gzip* или *snappy*, по умолчанию *none*
* **shutdownTimeoutMillis** - таймаут на доставку неотправленных сообщений из очереди, по умолчанию 1000
* **queueSize** - размер очереди, по умолчанию 1024 сообщения
* **mdcKeys** - перечень ключей, которые будут браться из Mapped Diagnostic Context, через запятую;
  в примере выше *mdcKeys=account_id*
    
Пример конфигурации в **logback.xml**:
```xml
<appender name="KAFKA" class="com.lognex.logging.KafkaLogbackAppender">
    <appname>hoard</appname>
    <hostname>localhost</hostname>
    <bootstrapServers>localhost:9092</bootstrapServers>
    <topicName>logs</topicName>
    <compressionType>lz4</compressionType>
    <mdcKeys>account_id,account_name,user_name</mdcKeys>
</appender>
```

Пример конфигурации в **standalone.xml**:
```xml
<custom-handler name="KAFKA" class="com.lognex.logging.KafkaJbossLogManagerHandler" module="com.lognex.logging">
    <level name="INFO"/>
    <properties>
        <property name="appname" value="exchange"/>
        <property name="hostname" value="localhost"/>
        <property name="bootstrapServers" value="localhost:9092"/>
        <property name="topicName" value="logs"/>
        <property name="compressionType" value="lz4"/>
        <property name="mdcKeys" value="account_id,account_name,user_name"/>
    </properties>
</custom-handler>
```

#### Тесты
Тестирование сейчас полуручное, полноценно автоматизировать долго и, наверное, нет смысла. Локально kafka проще всего
запустить в docker контейнере:
```bash
docker run --rm -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=192.168.50.237 --env ADVERTISED_PORT=9092 spotify/kafka
```
Смотреть сообщения можно через консольный клиент:
```bash
docker run --rm -it spotify/kafka bash
/opt/kafka_2.11-0.10.1.0/bin/kafka-console-consumer.sh --bootstrap-server 192.168.50.237:9092 --topic logs
```
Где 192.168.50.237 - это ip локальной машины.

#### TODO
* Обработка очень больших json-ов. Резать message или stackTrace.
* Копипаста между модулями.
* Ring buffer вместо ArrayBlockingQueue. Отбрасывать старые сообщения.
