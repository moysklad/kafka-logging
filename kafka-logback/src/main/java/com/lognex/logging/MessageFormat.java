package com.lognex.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.ThrowableProxyUtil;

import java.io.UnsupportedEncodingException;
import java.time.Instant;

final class MessageFormat {
    private MessageFormat() {}

    private static final String ENCODING = "UTF8";

    public static byte[] key(String env,
                             String app,
                             String host) throws UnsupportedEncodingException {
        return (env + "|" + app + "|" + host).getBytes(ENCODING);
    }

    public static byte[] body(ILoggingEvent event,
                              String env,
                              String app,
                              String host,
                              String[] mdcKeyArray) throws UnsupportedEncodingException {
        JsonWriter jw = new JsonWriter();
        jw.writeObjectOpen();
        jw.writeStringField("@timestamp", Instant.ofEpochMilli(event.getTimeStamp()).toString()); // ELK friendly name
        jw.writeStringField("env", env);
        jw.writeStringField("app", app);
        jw.writeStringField("host", host);
        jw.writeStringField("level", event.getLevel().toString());
        jw.writeStringField("logger", event.getLoggerName());
        jw.writeStringField("thread", event.getThreadName());
        for (String mdcKey : mdcKeyArray) {
            String mdcValue = event.getMDCPropertyMap().get(mdcKey);
            if (mdcValue != null) {
                jw.writeStringField(mdcKey, mdcValue);
            }
        }
        jw.writeStringField("message", event.getFormattedMessage());
        if (event.getThrowableProxy() != null) {
            jw.writeStringField("stackTrace", ThrowableProxyUtil.asString(event.getThrowableProxy()));
        }
        jw.writeObjectClose();
        return jw.toString().getBytes(ENCODING);
    }
}
