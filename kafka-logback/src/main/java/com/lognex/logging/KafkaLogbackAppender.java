package com.lognex.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.InterruptException;
import org.apache.kafka.common.serialization.ByteArraySerializer;

import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class KafkaLogbackAppender extends UnsynchronizedAppenderBase<ILoggingEvent> {

    private static final String[] IGNORED_PACKAGES = {
            "org.apache.kafka",
            "org.xerial.snappy"
    };

    private final Thread worker = new Worker();
    private Producer<byte[], byte[]> producer = null;
    private BlockingQueue<ProducerRecord<byte[], byte[]>> queue = null;

    private String env = null;
    private String app = null;
    private String host = null;
    private String bootstrapServers = null;
    private String topic = null;
    private String compressionType = null;
    private long shutdownTimeoutMillis = TimeUnit.SECONDS.toMillis(1);
    private int queueSize = 1024;
    private String mdcKeys = "";
    private String[] mdcKeyArray = new String[0];

    @Override
    public void start() {
        if (!isStarted()) {
            if (env == null || env.isEmpty()) {
                throw new IllegalArgumentException("env property is not set!");
            }
            if (app == null || app.isEmpty()) {
                throw new IllegalArgumentException("app property is not set!");
            }
            if (host == null || host.isEmpty()) {
                throw new IllegalArgumentException("host property is not set!");
            }
            if (bootstrapServers == null || bootstrapServers.isEmpty()) {
                throw new IllegalArgumentException("bootstrapServers property is not set!");
            }
            if (topic == null || topic.isEmpty()) {
                throw new IllegalArgumentException("topic property is not set!");
            }

            mdcKeyArray = mdcKeys.isEmpty() ? new String[0] : mdcKeys.split(",");

            Properties props = new Properties();
            props.put("client.id", host);
            props.put("bootstrap.servers", bootstrapServers);
            props.put("compression.type", compressionType == null ? "none" : compressionType);
            props.put("acks", "1");
            props.put("linger.ms", 10);
            props.put("buffer.memory", 33554432L);
            props.put("batch.size", 16384);
            props.put("max.in.flight.requests.per.connection", 5);
            props.put("key.serializer", ByteArraySerializer.class);
            props.put("value.serializer", ByteArraySerializer.class);
            producer = new KafkaProducer<>(props);

            queue = new ArrayBlockingQueue<>(queueSize);

            worker.setDaemon(true);
            worker.setName("Kafka-logging-async");
            worker.start();

            super.start();
        }
    }

    @Override
    public void stop() {
        if (isStarted()) {
            worker.interrupt();
            try {
                worker.join(shutdownTimeoutMillis);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
            super.stop();
        }
    }

    @Override
    protected void append(ILoggingEvent event) {
        for (String ignoredPackage : IGNORED_PACKAGES) {
            if (event.getLoggerName() != null && event.getLoggerName().startsWith(ignoredPackage)) {
                return;
            }
        }

        byte[] key;
        byte[] body;
        try {
            key = MessageFormat.key(env, app, host);
            body = MessageFormat.body(event, env, app, host, mdcKeyArray);
        } catch (Exception e) {
            addError("Json write error", e);
            return;
        }

        if (!queue.offer(new ProducerRecord<>(topic, key, body))) {
            addWarn("Queue overflow");
        }
    }

    private final class Worker extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    safeSend(queue.take());
                } catch (InterruptedException e) {
                    break;
                }
            }

            Iterator<ProducerRecord<byte[], byte[]>> i = queue.iterator();
            while (i.hasNext()) {
                safeSend(i.next());
                i.remove();
            }

            try {
                producer.flush();
                producer.close();
            } catch (Exception e) {
                addError("Failed to stop kafka producer", e);
            }
        }
    }

    private void safeSend(ProducerRecord<byte[], byte[]> record) {
        boolean interrupted = false;
        try {
            while (true) {
                try {
                    // mask interrupted flag
                    interrupted = Thread.interrupted();
                    producer.send(record);
                    break;
                } catch (InterruptException ie) {
                    // retry in case of interrupt, otherwise message is lost
                    interrupted = true;
                }
            }
        } catch (Exception e) {
            addError("Kafka send error", e);
        } finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBootstrapServers() {
        return bootstrapServers;
    }

    public void setBootstrapServers(String bootstrapServers) {
        this.bootstrapServers = bootstrapServers;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getCompressionType() {
        return compressionType;
    }

    public void setCompressionType(String compressionType) {
        this.compressionType = compressionType;
    }

    public long getShutdownTimeoutMillis() {
        return shutdownTimeoutMillis;
    }

    public void setShutdownTimeoutMillis(long shutdownTimeoutMillis) {
        this.shutdownTimeoutMillis = shutdownTimeoutMillis;
    }

    public int getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(int queueSize) {
        this.queueSize = queueSize;
    }

    public String getMdcKeys() {
        return mdcKeys;
    }

    public void setMdcKeys(String mdcKeys) {
        this.mdcKeys = mdcKeys;
    }
}
