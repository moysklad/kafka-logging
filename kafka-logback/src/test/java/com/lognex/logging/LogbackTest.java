package com.lognex.logging;

import ch.qos.logback.classic.LoggerContext;
import org.junit.After;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.UUID;

public class LogbackTest {

    @Test
    public void test() {
        MDC.put("account_id", new UUID(0L, 100L).toString());
        Logger logger = LoggerFactory.getLogger(LogbackTest.class);
        logger.debug("gazonk {}" , 1);
        logger.info("hello!");
        logger.error("aaa!", new Exception());
        MDC.clear();
    }

    @After
    public void after() {
        ((LoggerContext) LoggerFactory.getILoggerFactory()).stop();
    }
}
